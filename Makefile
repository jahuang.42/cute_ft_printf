NAME		= libftprintf.a
CC			= gcc
CFLAGS		= -Wall -Wextra -Werror
RM			= rm -rf
AR			= ar rcs

SRCS_DIR	= srcs/
LIBFT_DIR	= libft/
INCS_DIR	= includes

SRCS		= ft_printf.c \
			  ft_va_printf.c \
			  ft_set_specs.c \
			  ft_print_c.c \
			  ft_print_s.c \
			  ft_print_p.c \
			  ft_print_d.c \
			  ft_print_x.c \
			  ft_print_perc.c \
			  ft_print_ap.c \

LIBFT_SRCS	= ft_strlen.c \
			  ft_itoa_base.c \
			  ft_strchr_index.c \
			  ft_isdigit.c \
			  ft_putstr_fd.c \
			  ft_putstrn_fd.c \
			  ft_putcharn.c \

OBJS		= $(addprefix $(SRCS_DIR),$(SRCS:.c=.o))

LIBFT_OBJS	= $(addprefix $(LIBFT_DIR),$(LIBFT_SRCS:.c=.o))

%.o			: %.c
			$(CC) $(CFLAGS) -I $(INCS_DIR) -c $< -o $@

$(NAME)		: $(OBJS) $(LIBFT_OBJS) $(INCS)
			$(AR) $(NAME) $(OBJS) $(LIBFT_OBJS)

all			: $(NAME)

bonus		: $(NAME)

clean		:
			$(RM) $(OBJS) $(LIBFT_OBJS)

fclean		: clean
			$(RM) $(NAME)

re			: fclean all

.PHONY		: all clean fclean re
