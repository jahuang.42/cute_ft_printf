/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 12:24:29 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:25:59 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <stdio.h>

typedef struct	s_specs
{
	char		align;
	char		plus;
	int			zero;
	int			space;
	char		sign;
	int			hash;
	int			width;
	int			point;
	int			precision;
	char		type;
}				t_specs;

int				ft_printf(const char *f_string, ...);
int				ft_va_printf(const char *f_string, va_list ap);
int				ft_set_specs(t_specs *specs, const char *f_string, va_list ap);
int				ft_print_ap(va_list ap, t_specs specs);
int				ft_print_c(va_list ap, t_specs specs);
int				ft_print_s(va_list ap, t_specs specs);
int				ft_print_p(va_list ap, t_specs specs);
int				ft_print_d(va_list ap, t_specs specs);
int				ft_print_x(va_list ap, t_specs specs);
int				ft_print_perc(va_list ap, t_specs specs);

int				ft_strlen(char *s);
int				ft_strchr_index(char *s, char c);
char			*ft_itoa_base(unsigned long long nbr, char *base);
int				ft_isdigit(int c);
void			ft_putstr_fd(int fd, char *s);
void			ft_putstrn_fd(int fd, char *s, int n);
void			ft_putcharn(char c, int n);

#endif
