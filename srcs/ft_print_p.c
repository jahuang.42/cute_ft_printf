/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_p.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:17:27 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:14:43 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static t_specs	ft_specs_mod(t_specs specs)
{
	if (specs.width < 0)
	{
		specs.align = 1;
		specs.width *= -1;
	}
	return (specs);
}

static int		ft_printer(char *str, t_specs specs, int strlen)
{
	int		w_pad;
	int		printed;

	w_pad = 0;
	strlen += 2;
	if (specs.precision == 0 && str[0] == '0')
	{
		str[0] = '\0';
		strlen--;
	}
	if (specs.width > strlen)
		w_pad = specs.width - strlen;
	if (specs.align == 0)
		ft_putcharn(' ', w_pad);
	ft_putstr_fd(1, "0x");
	ft_putstr_fd(1, str);
	if (specs.align == 1)
		ft_putcharn(' ', w_pad);
	printed = strlen + w_pad;
	return (printed);
}

int				ft_print_p(va_list ap, t_specs specs)
{
	int					printed;
	unsigned long long	ptr;
	char				*str;

	ptr = va_arg(ap, unsigned long long);
	str = ft_itoa_base(ptr, "0123456789abcdef");
	specs = ft_specs_mod(specs);
	printed = ft_printer(str, specs, ft_strlen(str));
	return (printed);
}
