/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_ap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:15:10 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:19:34 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

int		ft_print_ap(va_list ap, t_specs specs)
{
	int printed_char;
	int	type_index;
	int (*printers[9])(va_list, t_specs);

	printed_char = 0;
	if (!specs.type)
		return (0);
	else
	{
		type_index = ft_strchr_index("cspdiuxX%", specs.type);
		printers[0] = ft_print_c;
		printers[1] = ft_print_s;
		printers[2] = ft_print_p;
		printers[3] = ft_print_d;
		printers[4] = ft_print_d;
		printers[5] = ft_print_d;
		printers[6] = ft_print_x;
		printers[7] = ft_print_x;
		printers[8] = ft_print_perc;
		printed_char = printers[type_index](ap, specs);
	}
	return (printed_char);
}
