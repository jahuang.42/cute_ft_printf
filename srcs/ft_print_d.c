/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_d.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:16:20 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 11:23:54 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static t_specs	ft_specs_mod(t_specs specs, int neg)
{
	if (specs.plus == 1)
		specs.space = 0;
	if (specs.width < 0)
	{
		specs.align = 1;
		specs.width *= -1;
	}
	if (specs.align == 1)
		specs.zero = 0;
	if (specs.point == 1 && specs.precision >= 0)
		specs.zero = 0;
	if (specs.plus == 1)
		specs.sign = '+';
	if (specs.space == 1)
		specs.sign = ' ';
	if (neg < 0)
		specs.sign = '-';
	return (specs);
}

static int		ft_set_p_pad(t_specs specs, int strlen)
{
	int p_pad;

	p_pad = 0;
	if (specs.precision > strlen)
		p_pad = specs.precision - strlen;
	return (p_pad);
}

static int		ft_set_w_pad(t_specs specs, int strlen, int p_pad)
{
	int w_pad;

	w_pad = 0;
	if (specs.width > p_pad + strlen)
		w_pad = specs.width - (p_pad + strlen);
	if (specs.sign != '\0' && w_pad != 0)
		w_pad--;
	return (w_pad);
}

static int		ft_printer(char *str, t_specs specs, int strlen)
{
	int		w_pad;
	int		p_pad;
	int		printed;

	(str[0] == '0' && specs.precision == 0) && (strlen = 0);
	p_pad = ft_set_p_pad(specs, strlen);
	w_pad = ft_set_w_pad(specs, strlen, p_pad);
	if (specs.align == 0 && specs.zero == 0)
		ft_putcharn(' ', w_pad);
	if (specs.sign != '\0')
		ft_putcharn(specs.sign, 1);
	if (specs.align == 0 && specs.zero == 1)
		ft_putcharn('0', w_pad);
	ft_putcharn('0', p_pad);
	if (!(str[0] == '0' && specs.precision == 0))
		ft_putstr_fd(1, str);
	if (specs.align == 1)
		ft_putcharn(' ', w_pad);
	printed = strlen + w_pad + p_pad;
	if (specs.sign != '\0')
		printed++;
	return (printed);
}

int				ft_print_d(va_list ap, t_specs specs)
{
	long long			number;
	int					negative;
	int					printed;
	unsigned long long	toa_number;
	char				*str;

	number = (long long)va_arg(ap, long long);
	if (specs.type == 'd' || specs.type == 'i')
		number = (int)number;
	else
		number = (unsigned int)number;
	negative = 1;
	toa_number = (unsigned long long)number;
	if (number < 0)
	{
		toa_number = (unsigned long long)(number + 1) * -1 + 1;
		negative = -1;
	}
	str = ft_itoa_base(toa_number, "0123456789");
	specs = ft_specs_mod(specs, negative);
	printed = ft_printer(str, specs, ft_strlen(str));
	return (printed);
}
