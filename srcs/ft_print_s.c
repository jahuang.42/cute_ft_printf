/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_s.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:17:50 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 11:27:47 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static t_specs	ft_specs_mod(t_specs specs)
{
	if (specs.width < 0)
	{
		specs.align = 1;
		specs.width *= -1;
	}
	if (specs.precision < 0)
		specs.point = 0;
	return (specs);
}

static int		ft_printer(char *str, t_specs specs, int strlen)
{
	int		w_pad;
	int		printed;

	w_pad = 0;
	if (specs.point == 1 && specs.precision < strlen)
		strlen = specs.precision;
	if (specs.width > strlen)
		w_pad = specs.width - strlen;
	if (specs.align == 0 && specs.zero == 1)
		ft_putcharn('0', w_pad);
	if (specs.align == 0 && specs.zero == 0)
		ft_putcharn(' ', w_pad);
	ft_putstrn_fd(1, str, strlen);
	if (specs.align == 1)
		ft_putcharn(' ', w_pad);
	printed = strlen + w_pad;
	return (printed);
}

int				ft_print_s(va_list ap, t_specs specs)
{
	int		printed;
	char	*str;

	specs = ft_specs_mod(specs);
	str = va_arg(ap, char *);
	if (!str)
		str = "(null)";
	printed = ft_printer(str, specs, ft_strlen(str));
	return (printed);
}
