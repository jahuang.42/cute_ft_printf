/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:18:38 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:09:47 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static t_specs	ft_specs_mod(t_specs specs, char *str)
{
	if (specs.plus == 1)
		specs.space = 0;
	if (specs.width < 0)
	{
		specs.align = 1;
		specs.width *= -1;
	}
	if (specs.align == 1)
		specs.zero = 0;
	if (specs.point == 1 && specs.precision >= 0)
		specs.zero = 0;
	if (str[0] == '0')
		specs.hash = 0;
	return (specs);
}

static int		ft_set_p_pad(t_specs specs, int strlen)
{
	int p_pad;

	p_pad = 0;
	if (specs.precision > strlen)
		p_pad = specs.precision - strlen;
	return (p_pad);
}

static int		ft_set_w_pad(t_specs specs, int strlen, int p_pad)
{
	int w_pad;

	w_pad = 0;
	if (specs.width > p_pad + strlen)
		w_pad = specs.width - (p_pad + strlen);
	return (w_pad);
}

static int		ft_printer(char *str, t_specs specs, int strlen)
{
	int		w_pad;
	int		p_pad;
	int		printed;
	char	*hash;

	hash = ((specs.type == 'x') ? "0x" : "0X");
	if (str[0] == '0' && specs.precision == 0)
		strlen = 0;
	p_pad = ft_set_p_pad(specs, strlen);
	if (specs.hash == 1)
		strlen += 2;
	w_pad = ft_set_w_pad(specs, strlen, p_pad);
	if (specs.align == 0 && specs.zero == 0)
		ft_putcharn(' ', w_pad);
	if (specs.hash == 1)
		ft_putstr_fd(1, hash);
	if (specs.align == 0 && specs.zero == 1)
		ft_putcharn('0', w_pad);
	ft_putcharn('0', p_pad);
	if (!(str[0] == '0' && specs.precision == 0))
		ft_putstr_fd(1, str);
	if (specs.align == 1)
		ft_putcharn(' ', w_pad);
	printed = strlen + w_pad + p_pad;
	return (printed);
}

int				ft_print_x(va_list ap, t_specs specs)
{
	unsigned int		number;
	char				*str;
	int					printed;

	number = (unsigned int)va_arg(ap, unsigned int);
	if (specs.type == 'x')
		str = ft_itoa_base((unsigned long long)number, "0123456789abcdef");
	else
		str = ft_itoa_base((unsigned long long)number, "0123456789ABCDEF");
	specs = ft_specs_mod(specs, str);
	printed = ft_printer(str, specs, ft_strlen(str));
	return (printed);
}
