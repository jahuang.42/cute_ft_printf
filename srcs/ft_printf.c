/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:18:45 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:20:35 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

int		ft_printf(const char *f_string, ...)
{
	va_list	ap;
	int		printed_char;

	printed_char = 0;
	va_start(ap, f_string);
	printed_char += ft_va_printf(f_string, ap);
	va_end(ap);
	return (printed_char);
}
