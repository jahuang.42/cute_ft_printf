/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:15:37 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:15:53 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static t_specs	ft_specs_mod(t_specs specs)
{
	if (specs.width < 0)
	{
		specs.align = 1;
		specs.width *= -1;
	}
	return (specs);
}

static int		ft_printer(char c, t_specs specs)
{
	int		w_pad;
	int		printed;

	w_pad = 0;
	if (specs.width > 1)
		w_pad = specs.width - 1;
	if (specs.align == 0)
		ft_putcharn(' ', w_pad);
	write(1, &c, 1);
	if (specs.align == 1)
		ft_putcharn(' ', w_pad);
	printed = 1 + w_pad;
	return (printed);
}

int				ft_print_c(va_list ap, t_specs specs)
{
	char	c;
	int		printed;

	c = va_arg(ap, int);
	specs = ft_specs_mod(specs);
	printed = ft_printer(c, specs);
	return (printed);
}
