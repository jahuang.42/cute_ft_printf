/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_va_printf.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:19:09 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:12:56 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

int		ft_va_printf(const char *f_string, va_list ap)
{
	int		printed_char;
	t_specs	specs;

	printed_char = 0;
	while (*f_string)
	{
		if (*f_string == '%')
		{
			f_string++;
			f_string += ft_set_specs(&specs, f_string, ap);
			if (specs.type)
				printed_char += ft_print_ap(ap, specs);
		}
		else
		{
			printed_char += write(1, f_string, 1);
			f_string++;
		}
	}
	return (printed_char);
}
