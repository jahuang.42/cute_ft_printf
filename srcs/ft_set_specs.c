/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_set_specs.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 11:18:56 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 11:20:50 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

static void	ft_init_specs(t_specs *specs)
{
	specs->align = 0;
	specs->plus = 0;
	specs->zero = 0;
	specs->space = 0;
	specs->sign = '\0';
	specs->hash = 0;
	specs->width = 0;
	specs->point = 0;
	specs->precision = -1;
	specs->type = 0;
}

static int	ft_set_flags(const char *f_string, t_specs *specs)
{
	int	len;

	len = 0;
	while (f_string[len] && ft_strchr_index("-+0 #", f_string[len]) > -1)
	{
		if (f_string[len] == '-')
			specs->align = 1;
		if (f_string[len] == '+')
			specs->plus = 1;
		if (f_string[len] == '0')
			specs->zero = 1;
		if (f_string[len] == ' ')
			specs->space = 1;
		if (f_string[len] == '#')
			specs->hash = 1;
		len++;
	}
	return (len);
}

static int	ft_set_width(const char *f_string, t_specs *specs, va_list ap)
{
	int	len;

	len = 0;
	if (f_string[len] == '*')
	{
		specs->width = va_arg(ap, int);
		return (len + 1);
	}
	while (ft_isdigit(f_string[len]))
	{
		specs->width = specs->width * 10 + f_string[len] - '0';
		len++;
	}
	return (len);
}

static int	ft_set_precision(const char *f_string, t_specs *specs, va_list ap)
{
	int		len;

	len = 0;
	if (f_string[len] == '.')
	{
		specs->point = 1;
		specs->precision = 0;
		len++;
		if (f_string[len] == '*')
		{
			specs->precision = va_arg(ap, int);
			return (len + 1);
		}
		while (ft_isdigit(f_string[len]))
		{
			specs->precision = specs->precision * 10 + f_string[len] - '0';
			len++;
		}
	}
	return (len);
}

int			ft_set_specs(t_specs *specs, const char *f_string, va_list ap)
{
	char	*types;
	int		len;

	types = "cspiudxX%";
	len = 0;
	ft_init_specs(specs);
	len += ft_set_flags(f_string, specs);
	len += ft_set_width(f_string + len, specs, ap);
	len += ft_set_precision(f_string + len, specs, ap);
	specs->type = (char)types[ft_strchr_index(types, f_string[len])];
	if (!specs->type)
		return (len);
	len++;
	return (len);
}
