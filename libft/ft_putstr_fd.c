/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 12:21:57 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:21:57 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

void	ft_putstr_fd(int fd, char *s)
{
	int index;

	index = 0;
	if (!s)
		return ;
	while (s[index])
	{
		write(fd, &s[index], 1);
		index++;
	}
	return ;
}
