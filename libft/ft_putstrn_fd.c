/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstrn_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 12:21:43 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:21:44 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

void	ft_putstrn_fd(int fd, char *s, int n)
{
	int index;

	index = 0;
	if (!s)
		return ;
	while (s[index] && index < n)
	{
		write(fd, &s[index], 1);
		index++;
	}
	return ;
}
