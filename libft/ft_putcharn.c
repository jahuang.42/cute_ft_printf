/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putcharn.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jahuang <jahuang@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/27 12:21:32 by jahuang           #+#    #+#             */
/*   Updated: 2021/04/27 12:21:33 by jahuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_printf.h"

void	ft_putcharn(char c, int n)
{
	int	index;

	index = 0;
	while (index < n)
	{
		write(1, &c, 1);
		index++;
	}
	return ;
}
